﻿namespace SolrConfigurator.BL {
    public static class Constants {

        /// <summary>
        /// The Id of the Sitecore Template Template
        /// </summary>
        public const string SitecoreTemplateIdString = "ab86861a-6030-46c5-b394-e8f99e8b87db";

        /// <summary>
        /// The Id of the Sitecore Field Template
        /// </summary>
        public const string SitecoreFieldIdString = "455a3e98-a627-4b40-8035-e683a0331ac7";

        /// <summary>
        /// The name of the template key used in yaml files
        /// </summary>
        public const string YamlKeyTemplate = "Template";

        /// <summary>
        /// The name of the id key used in yaml files
        /// </summary>
        public const string YamlKeyId = "ID";

        /// <summary>
        /// The name of the path key used in yaml files
        /// </summary>
        public const string YamlKeyPath = "Path";

        /// <summary>
        /// The search pattern used to get all .yml files
        /// </summary>
        public const string YamlSearchPattern = "*.yml";

        /// <summary>
        /// The search pattern used to get all .yml files
        /// </summary>
        public const string DefaultConfigFolder = "DefaultConfigs";

        /// <summary>
        /// The search pattern used to get all .yml files
        /// </summary>
        public const string DefaultConfigName = "SolrConfiguration.xml";

        /// <summary>
        /// A string containing the word true
        /// </summary>
        public const string TrueString = "true";
        
        /// <summary>
        /// A string containing the word false
        /// </summary>
        public const string FalseString = "false";
        
        /// <summary>
        /// The name of the placeholder for the indexAllFields value
        /// </summary>
        public const string PlaceholderIndexAllFields = "##Placeholder-IndexAllFields##";
                
        /// <summary>
        /// The name of the placeholder for the excludedTemplates value
        /// </summary>
        public const string PlaceholderExcludedTemplates = "##Placeholder-ExcludedTemplates##";
                
        /// <summary>
        /// The name of the placeholder for the includedTemplates value
        /// </summary>
        public const string PlaceholderIncludedTemplates = "##Placeholder-IncludedTemplates##";
                
        /// <summary>
        /// The name of the placeholder for the includedFields value
        /// </summary>
        public const string PlaceholderIncludedFields = "##Placeholder-IncludedFields##";
                
        /// <summary>
        /// The name of the placeholder for the excludedFields value
        /// </summary>
        public const string PlaceholderExcludedFields = "##Placeholder-ExcludedFields##";
                
        /// <summary>
        /// The indent used to create a clean config
        /// </summary>
        public const string ConfigIndent = "              ";
                
        /// <summary>
        /// The default folder name for the generated configs
        /// </summary>
        public const string GeneratedConfigFolder = "GeneratedConfigs";
                
        /// <summary>
        /// The default file name for the generated configs
        /// </summary>
        public const string GeneratedConfigName = "SolrConfiguration.xml";

        /// <summary>
        /// The name of the temp data key used to store/retrieve the data path
        /// </summary>
        public const string TempDataKeyDataPath = "DataPath";

        /// <summary>
        /// The name of the temp data key used to store/retrieve the selection viewModel
        /// </summary>
        public const string TempDataKeySelectionViewModel = "SelectionViewModel";

        /// <summary>
        /// The name of the temp data key used to store/retrieve the generated config
        /// </summary>
        public const string TempDataKeyGeneratedConfig = "GeneratedConfig";

        /// <summary>
        /// The name of the json key used for the text
        /// </summary>
        public const string JsonKeyText = "text";

        /// <summary>
        /// The name of the json key used for the value
        /// </summary>
        public const string JsonKeyValue = "value";

        /// <summary>
        /// The name of the json key used for the name
        /// </summary>
        public const string JsonKeyName = "name";

        /// <summary>
        /// The name of application type octet stream
        /// </summary>
        public const string ApplicationTypeOctetStream = "APPLICATION/octet-stream";

        /// <summary>
        /// The regex pattern used for name validation
        /// </summary>
        public const string NameValidationRegexPattern = "[^0-9a-zA-Z-_]+";

        /// <summary>
        /// The replacement used for name validation
        /// </summary>
        public const string NameValidationReplacement = "_";
    }
}

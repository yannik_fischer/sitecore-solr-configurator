﻿using System;
using System.IO.Abstractions;
using System.Text;
using System.Text.RegularExpressions;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.Models.Export;

namespace SolrConfigurator.BL.Managers {
    public class ExportHandler : IExportHandler {

        private readonly IFileSystem _fileSystem;
        private readonly IDirectory _directory;

        public ExportHandler() : this(new FileSystem()) { }

        public ExportHandler(IFileSystem fileSystem) {
            _fileSystem = fileSystem;
            _directory = new DirectoryWrapper(_fileSystem);
        }

        /// <summary>
        /// Uses the default config and replaces the placeholder with the model
        /// </summary>
        /// <param name="model"></param>
        /// <returns>A string of the default config with replaced placeholders</returns>
        public string MapModelToFileString(ExportModel model) {
            try {
                if (model == null) {
                    return string.Empty;
                }
                var configString = _fileSystem.File.ReadAllText($"{Constants.DefaultConfigFolder}/{Constants.DefaultConfigName}");

                var indexAllFields = model.IndexAllFields ? Constants.TrueString : Constants.FalseString;
                configString = configString.Replace(Constants.PlaceholderIndexAllFields, indexAllFields);

                if (model.ExcludedTemplates != null) {
                    var excludedTemplates = new StringBuilder();
                    foreach (var eT in model.ExcludedTemplates) {
                        excludedTemplates.AppendLine($"{Constants.ConfigIndent}<{ValidateName(eT.Name)}>{eT.Value}</{ValidateName(eT.Name)}>");
                    }
                    configString = configString.Replace($"{Constants.PlaceholderExcludedTemplates}\r\n", excludedTemplates.ToString());
                }

                if (model.IncludedTemplates != null) {
                    var includedTemplates = new StringBuilder();
                    foreach (var iT in model.IncludedTemplates) {
                        includedTemplates.AppendLine($"{Constants.ConfigIndent}<{ValidateName(iT.Name)}>{iT.Value}</{ValidateName(iT.Name)}>");
                    }
                    configString = configString.Replace($"{Constants.PlaceholderIncludedTemplates}\r\n", includedTemplates.ToString());
                }

                if (model.IncludedFields != null) {
                    var includedFields = new StringBuilder();
                    foreach (var iF in model.IncludedFields) {
                        includedFields.AppendLine($"{Constants.ConfigIndent}<{ValidateName(iF.Name)}>{iF.Value}</{ValidateName(iF.Name)}>");
                    }
                    configString = configString.Replace($"{Constants.PlaceholderIncludedFields}\r\n", includedFields.ToString());
                }

                if (model.ExcludedFields != null) {
                    var excludedFields = new StringBuilder();
                    foreach (var eF in model.ExcludedFields) {
                        excludedFields.AppendLine($"{Constants.ConfigIndent}<{ValidateName(eF.Name)}>{eF.Value}</{ValidateName(eF.Name)}>");
                    }
                    configString = configString.Replace($"{Constants.PlaceholderExcludedFields}\r\n", excludedFields.ToString());
                }

                return configString;

            } catch (Exception) {
                return string.Empty;
            }
        }

        /// <summary>
        /// Generates a .xml file based on the xmlString
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns>The file name of the generated file</returns>
        public string ExportFileStringToFile(string xmlString) {
            try {
                if (!_directory.Exists(Constants.GeneratedConfigFolder)) {
                    _directory.CreateDirectory(Constants.GeneratedConfigFolder);
                }

                var fileName = $"{DateTime.Now:yyyyMMdd-hhmmss}_{Constants.GeneratedConfigName}";
                var path = $"{Constants.GeneratedConfigFolder}/{fileName}";
                _fileSystem.File.WriteAllText(path, xmlString);
                if (_fileSystem.File.Exists(path)) {
                    return fileName;
                }

                return string.Empty;

            } catch (Exception) {
                return string.Empty;
            }
        }

        /// <summary>
        /// Replaces invalid characters in the input string
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A valid string without special characters</returns>
        private static string ValidateName(string input) {
            return Regex.Replace(input, Constants.NameValidationRegexPattern, Constants.NameValidationReplacement);
        }
    }
}

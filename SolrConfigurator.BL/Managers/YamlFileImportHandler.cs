﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.Models.Serialization;
using YamlDotNet.RepresentationModel;
using Type = SolrConfigurator.Models.Enums.Type;

namespace SolrConfigurator.BL.Managers {
    public class YamlFileImportHandler : IImportHandler {
        private readonly IFileSystem _fileSystem;

        public YamlFileImportHandler() : this(new FileSystem()) { }

        public YamlFileImportHandler(IFileSystem fileSystem) {
            _fileSystem = fileSystem;
        }

        /// <summary>
        /// Imports all .yml files in the specified folder and all of it's subfolders and returns all templates and fields found inside
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>Returns a list of SerializedFiles</returns>
        public List<SerializedFile> ImportFiles(string filePath) {
            try {
                if (string.IsNullOrWhiteSpace(filePath) || !Directory.Exists(filePath)) {
                    return null;
                }

                var di = _fileSystem.DirectoryInfo.FromDirectoryName(filePath);
                var files = di.GetFiles(Constants.YamlSearchPattern, SearchOption.AllDirectories);

                var serializedFiles = new List<SerializedFile>();
                foreach (var file in files) {
                    try {
                        using var reader = new StreamReader(_fileSystem.FileStream.Create(file.FullName, FileMode.Open));
                        var yaml = new YamlStream();
                        yaml.Load(reader);

                        foreach (var yamlDocument in yaml.Documents) {
                            var root = (YamlMappingNode)yamlDocument.RootNode;
                            if (root != null) {
                                var yamlType = root.Children
                                    .Where(x => ((YamlScalarNode)x.Key).Value == Constants.YamlKeyTemplate)
                                    .Select(x => ((YamlScalarNode)x.Value).Value).FirstOrDefault();
                                //Check if yamlType is Template or Field, if it's neither don't add it to the list
                                Type type;
                                if (yamlType == Constants.SitecoreTemplateIdString) {
                                    type = Type.Template;
                                } else if (yamlType == Constants.SitecoreFieldIdString) {
                                    type = Type.Field;
                                } else {
                                    continue;
                                }

                                var yamlId = root.Children.Where(x => ((YamlScalarNode)x.Key).Value == Constants.YamlKeyId)
                                    .Select(x => ((YamlScalarNode)x.Value).Value).FirstOrDefault();
                                var yamlPath = root.Children
                                    .Where(x => ((YamlScalarNode)x.Key).Value == Constants.YamlKeyPath)
                                    .Select(x => ((YamlScalarNode)x.Value).Value).FirstOrDefault();
                                var name = yamlPath?.Split("/").LastOrDefault();

                                serializedFiles.Add(new SerializedFile {
                                    Name = string.IsNullOrEmpty(name) ? yamlId : name,
                                    Path = yamlPath,
                                    Value = yamlId,
                                    Type = type
                                });
                            }
                        }
                    } catch (Exception) {
                        // ignored
                    }
                }
                return serializedFiles;

            } catch (Exception) {
                return null;
            }
        }
    }
}
﻿using System.Collections.Generic;
using SolrConfigurator.Models.Serialization;

namespace SolrConfigurator.BL.Interfaces {
    public interface IImportHandler {
        List<SerializedFile> ImportFiles(string filePath);
    }
}

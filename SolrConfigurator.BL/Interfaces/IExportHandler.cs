﻿using SolrConfigurator.Models.Export;

namespace SolrConfigurator.BL.Interfaces {
    public interface IExportHandler {
        string MapModelToFileString(ExportModel model);
        string ExportFileStringToFile(string xmlString);
    }
}

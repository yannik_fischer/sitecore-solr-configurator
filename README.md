# Sitecore SOLR configurator
**Graduation project by Yannik Fischer**

**This documentation contains all necessary information on how to install, run, use and update the web application.**
***
## Goal
This web application was created in order to reduce the steps necessary to create a Solr index configuration.

Instead of having to search for the Id of a field on the corresponding template you can instead use this web application.
This is especially helpful for creating the initial configuration where you would have to repeat this process for each field you want to add.

## Prerequisites
- [Sitecore](https://dev.sitecore.net/) - tested in version 9.0.2
- [Unicorn](https://github.com/SitecoreUnicorn/Unicorn) has to be installed and set up
- [.Net Core Runtime](https://dotnet.microsoft.com/download/dotnet-core/2.2) on the computer hosting the app
- [Visual Studio](https://visualstudio.microsoft.com/) on your development environment
- [Internet Information Services (IIS)](https://www.iis.net/) to host the application

## How to install
1. Clone this repository to your development environment
1. Create a new Website in your IIS
1. Open the Visual Studio solution and create a new publish target. Point the target to your new website
1. Deploy the application

## How to run
- To run the web application you have two choices:

1. Run the application from visual studio by either using the debugger or run without debugging
1. Open the website in your browser of choice

## How to use
- The application guides you through all the necessary steps to generate your Solr index configuration
- All you have to do is to follow the instructions on the screen:

1. Specify the path where your .yml files are located
1. Once all files have been loaded make your selection based on your needs
1. Check if everything is correct in the preview window and download your configuration

## How to extend
- If any changes have to be made to this programm you should follow this best practise:

1. Create a new feature branch from the current development branch
1. Make your changes and commit them to your feature branch
1. Create a new pull request and add the necessary reviewers
1. Once they approve of your changes, merge your pull request back into develop
1. Create a new release branch from the current development branch and deploy it to your testing environment
1. If all tests succeed merge your release branch into master

## Other
* Link to the [Jira Project](https://jira.kuehlhaus.com/projects/AYSSC/summary)
﻿using Microsoft.Extensions.DependencyInjection;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.BL.Managers;

namespace SolrConfigurator.DI {
    public static class DependencyInjection {
        public static void ConfigureServices(IServiceCollection services) {
            services.AddSingleton<IImportHandler, YamlFileImportHandler>();
            services.AddSingleton<IExportHandler, ExportHandler>();
        }
    }
}

﻿if (document.getElementById("app")) {

    const app = new Vue({
        el: '#app',
        data: {
            successUrl: "",
            search: "",
            allData: [{ "text": "Loading...", "value": "0000" }],
            selectedAllDataValues: [],
            selectedITValues: [],
            selectedIFValues: [],
            selectedETValues: [],
            selectedEFValues: [],
            includedTemplates: [],
            includedFields: [],
            excludedTemplates: [],
            excludedFields: [],
            indexAllFields: false,
            result: ""
        },
        mounted: function () {
            this.successUrl = this.$el.dataset.successurl;
            axios
                .get('/Selection/GetData')
                .then(response => (this.allData = response.data));
        },
        computed: {
            filteredAllDataValues() {
                return this.allData.filter(option => {
                    return option.text.toLowerCase().includes(this.search.toLowerCase());
                });
            }
        },
        methods: {
            moveToArray: function (array) {
                for (data of this.allData) {
                    for (selectedValue of this.selectedAllDataValues) {
                        if (data.value === selectedValue) {
                            array.push(data);
                            data.hidden = true;
                        }
                    }
                }
            },
            removeFromArray: function (arrayToSplice, arraySelectedSource) {
                for (data of this.allData) {
                    for (selectedValue of arraySelectedSource) {
                        if (data.value === selectedValue) {
                            arrayToSplice.splice(arrayToSplice.indexOf(data), 1);
                            data.hidden = false;
                        }
                    }
                }
            },
            addIt: function () {
                this.moveToArray(this.includedTemplates);
            },
            addIf: function () {
                this.moveToArray(this.includedFields);
            },
            addEt: function () {
                this.moveToArray(this.excludedTemplates);
            },
            addEf: function () {
                this.moveToArray(this.excludedFields);
            },
            remIt: function () {
                this.removeFromArray(this.includedTemplates, this.selectedITValues);
            },
            remIf: function () {
                this.removeFromArray(this.includedFields, this.selectedIFValues);
            },
            remEt: function () {
                this.removeFromArray(this.excludedTemplates, this.selectedETValues);
            },
            remEf: function () {
                this.removeFromArray(this.excludedFields, this.selectedEFValues);
            },
            sendData: function () {
                var self = this;
                var objectToSend = {
                    indexAllFields: this.indexAllFields,
                    includedTemplates: this.includedTemplates.map((it) => { return { value: it.value, name: it.name }; }),
                    includedFields: this.includedFields.map((incf) => { return { value: incf.value, name: incf.name }; }),
                    excludedTemplates: this.excludedTemplates.map((et) => { return { value: et.value, name: et.name }; }),
                    excludedFields: this.excludedFields.map((ef) => { return { value: ef.value, name: ef.name }; })
                };

                axios.post('/Selection/PostData', objectToSend).then(function () { location.href = self.successUrl });
                console.log(objectToSend);
                this.result = JSON.stringify(objectToSend, null, 4);
            }
        }
    });
}
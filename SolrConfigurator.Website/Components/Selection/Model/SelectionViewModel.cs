﻿using System.Collections.Generic;
using SolrConfigurator.Models.Serialization;

namespace SolrConfigurator.Website.Components.Selection.Model {
    public class SelectionViewModel {

        /// <summary>
        /// Gets or sets the index all fields flag
        /// </summary>
        public bool IndexAllFields { get; set; }

        /// <summary>
        /// Gets or sets the templates that are included
        /// </summary>
        public List<SerializedFile> IncludedTemplates { get; set; }

        /// <summary>
        /// Gets or sets the templates that are excluded
        /// </summary>
        public List<SerializedFile> ExcludedTemplates { get; set; }

        /// <summary>
        /// Gets or sets the fields that are included
        /// </summary>
        public List<SerializedFile> IncludedFields { get; set; }

        /// <summary>
        /// Gets or sets the fields that are excluded
        /// </summary>
        public List<SerializedFile> ExcludedFields { get; set; }
    }
}

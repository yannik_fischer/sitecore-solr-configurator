﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using SolrConfigurator.BL;
using SolrConfigurator.BL.Extensions;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.Website.Components.DataPath.Model;
using SolrConfigurator.Website.Components.Selection.Model;
using SolrConfigurator.Website.Components.Shared.Models;

namespace SolrConfigurator.Website.Components.Selection.Controller {
    public class SelectionController : Microsoft.AspNetCore.Mvc.Controller {
        private readonly IImportHandler _importHandler;

        public SelectionController(IImportHandler importHandler) {
            _importHandler = importHandler;
        }

        /// <summary>
        /// Processes the data path and returns the selection view
        /// </summary>
        /// <param name="dataPathViewModel">Used to save the path to temp data</param>
        /// <returns>IActionResult View</returns>
        [HttpPost]
        public IActionResult ProcessDataPath(DataPathViewModel dataPathViewModel) {
            if (string.IsNullOrEmpty(dataPathViewModel.Path)) {
                return View("~/Components/Shared/Views/Error.cshtml", new ErrorViewModel("dataPathViewModel.Path is null or empty"));
            }

            TempData.Put(Constants.TempDataKeyDataPath, dataPathViewModel.Path);

            return View("~/Components/Selection/View/Selection.cshtml");
        }
        
        /// <summary>
        /// Retrieves the data path from temp data and returns a json string containing the data
        /// </summary>
        /// <returns>Json string</returns>
        [HttpGet]
        public string GetData() {
            var dataPath = TempData.Get<string>(Constants.TempDataKeyDataPath);
            if (string.IsNullOrEmpty(dataPath)) {
                return null;
            }

            var data = _importHandler.ImportFiles(dataPath);
            if (data == null) {
                return null;
            }

            var json = new JArray(
                data.Select(f => new JObject {
                        {Constants.JsonKeyText, $"{f.Type} - {f.Path}"},
                        {Constants.JsonKeyValue, f.Value},
                        {Constants.JsonKeyName, f.Name }
                })
                );

            return json.ToString();
        }

        /// <summary>
        /// Saves the viewModel to temp data
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>IActionResult Ok</returns>
        [HttpPost]
        public IActionResult PostData([FromBody] SelectionViewModel viewModel) {
            if (viewModel == null) {
                return StatusCode(500);
            }

            TempData.Put(Constants.TempDataKeySelectionViewModel, viewModel);
            
            return StatusCode(200);
        }
    }
}
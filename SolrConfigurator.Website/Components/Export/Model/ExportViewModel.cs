﻿namespace SolrConfigurator.Website.Components.Export.Model {
    public class ExportViewModel {
        
        /// <summary>
        /// Gets or sets the generated config
        /// </summary>
        public string GeneratedConfig { get; set; }
    }
}

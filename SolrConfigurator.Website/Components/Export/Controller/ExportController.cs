﻿using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using SolrConfigurator.BL;
using SolrConfigurator.BL.Extensions;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.Models.Export;
using SolrConfigurator.Website.Components.Export.Model;
using SolrConfigurator.Website.Components.Selection.Model;
using SolrConfigurator.Website.Components.Shared.Models;

namespace SolrConfigurator.Website.Components.Export.Controller {
    public class ExportController : Microsoft.AspNetCore.Mvc.Controller {
        private readonly IExportHandler _exportHandler;

        public ExportController(IExportHandler exportHandler) {
            _exportHandler = exportHandler;
        }

        /// <summary>
        /// Processes the selection made in the selection view and generates a config string
        /// </summary>
        /// <returns>IActionResult View</returns>
        public IActionResult ProcessSelection() {
            var selectionViewModel = TempData.Get<SelectionViewModel>(Constants.TempDataKeySelectionViewModel);

            if (selectionViewModel == null) {
                return View("~/Components/Shared/Views/Error.cshtml", new ErrorViewModel("selectionViewModel is null"));
            }

            if ((selectionViewModel.IncludedTemplates == null || !selectionViewModel.IncludedTemplates.Any()) &&
                (selectionViewModel.IncludedFields == null || !selectionViewModel.IncludedFields.Any()) &&
                (selectionViewModel.ExcludedTemplates == null || !selectionViewModel.ExcludedTemplates.Any()) &&
                (selectionViewModel.ExcludedFields == null || !selectionViewModel.ExcludedFields.Any()) &&
                selectionViewModel.IndexAllFields == false) {
                return View("~/Components/Shared/Views/Error.cshtml", new ErrorViewModel("No selection was made and indexAllFields was false"));
            }

            var exportModel = new ExportModel {
                IndexAllFields = selectionViewModel.IndexAllFields,
                IncludedTemplates = selectionViewModel.IncludedTemplates,
                ExcludedTemplates = selectionViewModel.ExcludedTemplates,
                IncludedFields = selectionViewModel.IncludedFields,
                ExcludedFields = selectionViewModel.ExcludedFields
            };

            var generatedConfig = _exportHandler.MapModelToFileString(exportModel);
            if (string.IsNullOrEmpty(generatedConfig)) {
                return View("~/Components/Shared/Views/Error.cshtml", new ErrorViewModel("generatedConfig is null or empty"));
            }
            
            var exportViewModel = new ExportViewModel {
                GeneratedConfig = generatedConfig
            };

            TempData.Put(Constants.TempDataKeyGeneratedConfig, generatedConfig);

            return BindExport(exportViewModel);
        }

        public IActionResult BindExport(ExportViewModel viewModel) {
            return View("~/Components/Export/View/Export.cshtml", viewModel);
        }

        /// <summary>
        /// Generates a config from the config string and downloads the file
        /// </summary>
        /// <returns>IActionResult File</returns>
        [HttpGet]
        public IActionResult GetFileDownload() {
            var generatedConfig = TempData.Get<string>(Constants.TempDataKeyGeneratedConfig);
            if (string.IsNullOrEmpty(generatedConfig)) {
                return View("~/Components/Shared/Views/Error.cshtml", new ErrorViewModel("TempData[\"generatedConfig\"] is null or empty"));
            }

            var fileName = _exportHandler.ExportFileStringToFile(generatedConfig);
            if (string.IsNullOrEmpty(fileName)) {
                return View("~/Components/Shared/Views/Error.cshtml", new ErrorViewModel("filename is null or empty"));
            }

            var data = Encoding.ASCII.GetBytes(generatedConfig);

            return File(data, Constants.ApplicationTypeOctetStream, fileName);
        }
    }
}
﻿namespace SolrConfigurator.Website.Components.DataPath.Model {
    public class DataPathViewModel {

        /// <summary>
        /// Gets or sets the path for the yaml files
        /// </summary>
        public string Path { get; set; }
    }
}

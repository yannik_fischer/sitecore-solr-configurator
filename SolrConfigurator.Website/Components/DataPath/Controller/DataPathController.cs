﻿using Microsoft.AspNetCore.Mvc;
using SolrConfigurator.Website.Components.DataPath.Model;

namespace SolrConfigurator.Website.Components.DataPath.Controller {
    public class DataPathController : Microsoft.AspNetCore.Mvc.Controller {

        /// <summary>
        /// Bind the data path component
        /// </summary>
        /// <returns>IActionResult View</returns>
        public IActionResult BindDataPath() {
            var viewModel = new DataPathViewModel();
            return View("~/Components/DataPath/View/DataPath.cshtml", viewModel);
        }
    }
}
namespace SolrConfigurator.Website.Components.Shared.Models {
    public class ErrorViewModel {

        public ErrorViewModel(string message) {
            Message = message;
        }
        
        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string Message { get; set; }
    }
}

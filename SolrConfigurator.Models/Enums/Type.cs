﻿namespace SolrConfigurator.Models.Enums {

    /// <summary>
    /// Used to determine the type of file that was imported
    /// </summary>
    public enum Type {
        Template = 0,
        Field = 1
    }
}

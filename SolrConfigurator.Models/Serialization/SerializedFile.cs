﻿using Type = SolrConfigurator.Models.Enums.Type;

namespace SolrConfigurator.Models.Serialization {
    public class SerializedFile {
        
        /// <summary>
        /// Used to store the name of a serialized file
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Used to store the path of a serialized file
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Used to store the id of a serialized file
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Used to store the type of a serialized file
        /// </summary>
        public Type Type { get; set; }
    }
}

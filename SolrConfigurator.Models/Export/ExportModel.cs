﻿using System.Collections.Generic;
using SolrConfigurator.Models.Serialization;

namespace SolrConfigurator.Models.Export {
    public class ExportModel {

        /// <summary>
        /// Determines if all fields should be indexed or not
        /// </summary>
        public bool IndexAllFields { get; set; }

        /// <summary>
        /// A list of all templates to be included in the export
        /// </summary>
        public List<SerializedFile> IncludedTemplates { get; set; }

        /// <summary>
        /// A list of all templates to be excluded from the export
        /// </summary>
        public List<SerializedFile> ExcludedTemplates { get; set; }

        /// <summary>
        /// A list of all fields to be included in the export
        /// </summary>
        public List<SerializedFile> IncludedFields { get; set; }

        /// <summary>
        /// A list of all fields to be excluded from the export
        /// </summary>
        public List<SerializedFile> ExcludedFields { get; set; }
    }
}

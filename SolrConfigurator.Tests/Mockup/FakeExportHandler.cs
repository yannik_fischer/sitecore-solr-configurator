﻿using Castle.Core.Internal;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.Models.Export;

namespace SolrConfigurator.Tests.Mockup {
    public class FakeExportHandler : IExportHandler {

        private const string FileString = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><configuration xmlns:patch=\"http://www.sitecore.net/xmlconfig/\"></configuration>";

        public string MapModelToFileString(ExportModel model) {
            if (model == null) {
                return string.Empty;
            }

            return FileString;
        }

        public string ExportFileStringToFile(string xmlString) {
            if (xmlString.IsNullOrEmpty()) {
                return string.Empty;
            }

            return "C:/";
        }
    }
}

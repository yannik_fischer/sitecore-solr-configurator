﻿using System;
using System.Collections.Generic;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.Models.Serialization;
using Type = SolrConfigurator.Models.Enums.Type;

namespace SolrConfigurator.Tests.Mockup {
    public class FakeImportHandler : IImportHandler {
        private readonly List<SerializedFile> _files = new List<SerializedFile> {
            new SerializedFile {
                Name = "Entry 1",
                Path = "sitecore/templates/Entry 1",
                Type = Type.Template,
                Value = Guid.Empty.ToString("D")
            }, new SerializedFile {
                Name = "Entry 2",
                Path = "sitecore/templates/Entry 2",
                Type = Type.Template,
                Value = Guid.Empty.ToString("D")
            }, new SerializedFile {
                Name = "Entry 3",
                Path = "sitecore/templates/Entry 3",
                Type = Type.Template,
                Value = Guid.Empty.ToString("D")
            }, new SerializedFile {
                Name = "Entry 4",
                Path = "sitecore/templates/Entry 4",
                Type = Type.Template,
                Value = Guid.Empty.ToString("D")
            }
        };

        public List<SerializedFile> ImportFiles(string filePath) {
            if (string.IsNullOrEmpty(filePath)) {
                return new List<SerializedFile>();
            }

            return _files;
        }
    }
}

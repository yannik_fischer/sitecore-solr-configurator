﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using Castle.Core.Internal;
using NUnit.Framework;
using SolrConfigurator.BL;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.BL.Managers;
using SolrConfigurator.Models.Export;
using SolrConfigurator.Models.Serialization;
using SolrConfigurator.Tests.Controllers.Enums;
using Type = SolrConfigurator.Models.Enums.Type;

namespace SolrConfigurator.Tests.Managers {
    public class ExportHandlerTests {
        private IFileSystem _fileSystem;
        private readonly string _configString = $"{Constants.PlaceholderIndexAllFields}{Constants.PlaceholderExcludedTemplates}\r\n{Constants.PlaceholderIncludedTemplates}\r\n{Constants.PlaceholderIncludedFields}\r\n{Constants.PlaceholderExcludedFields}\r\n";
        private IExportHandler _exportHandler;

        [SetUp]
        public void Setup() {
            _fileSystem = new MockFileSystem(new Dictionary<string, MockFileData> {
                {$"{Constants.DefaultConfigFolder}/{Constants.DefaultConfigName}", new MockFileData(_configString)},
                {$"{Constants.GeneratedConfigFolder}/{DateTime.Now:yyyyMMdd-hhmmss}_{Constants.GeneratedConfigName}", new MockFileData(_configString)}
            });
            _exportHandler = new ExportHandler(_fileSystem);
        }

        [TestCase(ModelType.Null, "")]
        [TestCase(ModelType.Empty, Constants.FalseString)]
        [TestCase(ModelType.Populated, "true              <Entry_1>00000000-0000-0000-0000-000000000000</Entry_1>\r\n")]
        public void MapModelToFileString_ReturnsFileString(ModelType modelType, string returnedValue) {
            ExportModel exportModel = null;

            if (modelType == ModelType.Empty) {
                exportModel = new ExportModel {
                    IncludedTemplates = new List<SerializedFile>(),
                    ExcludedTemplates = new List<SerializedFile>(),
                    IncludedFields = new List<SerializedFile>(),
                    ExcludedFields = new List<SerializedFile>(),
                    IndexAllFields = false
                };
            } else if (modelType == ModelType.Populated) {
                exportModel = new ExportModel {
                    IncludedTemplates = new List<SerializedFile> {
                        new SerializedFile {
                            Name = "Entry 1",
                            Path = "sitecore/templates/Entry 1",
                            Type = Type.Template,
                            Value = Guid.Empty.ToString("D")
                        }
                    },
                    ExcludedTemplates = new List<SerializedFile>(),
                    IncludedFields = new List<SerializedFile>(),
                    ExcludedFields = new List<SerializedFile>(),
                    IndexAllFields = true
                };
            }

            var result = _exportHandler.MapModelToFileString(exportModel);

            Assert.That(string.Equals(result, returnedValue));
        }

        [Test]
        public void ExportFileStringToFile_ReturnsFilePath() {
            var result = _exportHandler.ExportFileStringToFile(_configString);

            Assert.That(!result.IsNullOrEmpty());
        }
    }
}

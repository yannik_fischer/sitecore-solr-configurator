﻿using System.Collections.Generic;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Linq;
using NUnit.Framework;
using SolrConfigurator.BL.Interfaces;
using SolrConfigurator.BL.Managers;

namespace SolrConfigurator.Tests.Managers {
    public class YamlFileImportHandlerTests {
        private IFileSystem _fileSystem;
        private readonly string _dummyYaml = "ID: \"00000000-0000-0000-0000-000000000000\"\r\nTemplate: \"ab86861a-6030-46c5-b394-e8f99e8b87db\"\r\nPath: /sitecore/templates/Test";
        private IImportHandler _importHandler;

        [SetUp]
        public void Setup() {
            _fileSystem = new MockFileSystem(new Dictionary<string, MockFileData> {
                {"C:/testFile1.yml", new MockFileData(_dummyYaml)},
                {"C:/testFile2.yml", new MockFileData(_dummyYaml)},
                {"C:/testFile3.yml", new MockFileData(_dummyYaml)},
            });
            _importHandler = new YamlFileImportHandler(_fileSystem);
        }

        [TestCase(null, false)] //Path is null
        [TestCase("", false)] //Path is empty
        [TestCase("C:/", true)] //Path is correct
        [TestCase("D:/", false)] //Path does not exist
        public void ImportFiles_ReturnsSerializedFiles(string path, bool expectedResult) {
            var result = _importHandler.ImportFiles(path);

            Assert.That((result != null && result.Any()) == expectedResult);
        }
    }
}

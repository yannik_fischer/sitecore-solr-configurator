﻿using System;
using Castle.Core.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using SolrConfigurator.BL;
using SolrConfigurator.BL.Extensions;
using SolrConfigurator.Tests.Mockup;
using SolrConfigurator.Website.Components.DataPath.Model;
using SolrConfigurator.Website.Components.Selection.Controller;
using SolrConfigurator.Website.Components.Selection.Model;

namespace SolrConfigurator.Tests.Controllers {

    [TestFixture]
    public class SelectionControllerTests {
        private HttpContext _httpContext;
        private TempDataDictionary _tempData;
        private SelectionController _controller;

        [SetUp]
        public void Setup() {
            _httpContext = new DefaultHttpContext();
            _tempData = new TempDataDictionary(_httpContext, Mock.Of<ITempDataProvider>());
            _controller = new SelectionController(new FakeImportHandler()) {
                TempData = _tempData
            };
        }

        [TestCase("~/Components/Selection/View/Selection.cshtml", "C:/", true)]
        [TestCase("~/Components/Selection/View/Selection.cshtml", null, false)]
        [TestCase("~/Components/Shared/Views/Error.cshtml", "C:/", false)]
        [TestCase("~/Components/Shared/Views/Error.cshtml", null, true)]
        public void ProcessDataPath_ReturnsExpectedView(string viewName, string path, bool expectedResult) {
            var viewModel = new DataPathViewModel {
                Path = path
            };

            var result = _controller.ProcessDataPath(viewModel) as ViewResult;

            Assert.NotNull(result);
            Assert.That(string.Equals(result.ViewName, viewName) == expectedResult);
        }

        [TestCase(null, false)]
        [TestCase("C:/", true)]
        public void GetData_ReturnsValue(string dataPath, bool expectedValue) {
            _tempData.Put(Constants.TempDataKeyDataPath, dataPath);

            var result = _controller.GetData();

            Assert.That(!string.IsNullOrEmpty(result) == expectedValue);
        }

        [TestCase("C:/")]
        public void GetData_ReturnsValidJson(string dataPath) {
            _tempData.Put(Constants.TempDataKeyDataPath, dataPath);

            var result = _controller.GetData();
            var isValid = true;

            try {
                JToken.Parse(result);
            } catch (Exception) {
                isValid = false;
            }

            Assert.IsTrue(isValid);
        }

        [TestCase(null, 500)]
        [TestCase("{\"indexAllFields\":true,\"excludedFields\":[{\"value\":\"4333e889-ecbd-4517-9223-fe266bca86be\",\"name\":\"IsMicrosite\"}]}", 200)]
        public void PostData_ReturnsExpectedStatusCode(string json, int expectedResult) {
            SelectionViewModel viewModel = null;
            if (!json.IsNullOrEmpty()) {
                viewModel = Newtonsoft.Json.JsonConvert.DeserializeObject<SelectionViewModel>(json);
            }

            var result = _controller.PostData(viewModel) as StatusCodeResult;

            Assert.NotNull(result);
            Assert.That(result.StatusCode == expectedResult);
        }
    }
}

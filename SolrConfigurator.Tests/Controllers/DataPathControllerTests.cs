﻿using Microsoft.AspNetCore.Mvc;
using SolrConfigurator.Website.Components.DataPath.Controller;
using NUnit.Framework;
using SolrConfigurator.Website.Components.DataPath.Model;

namespace SolrConfigurator.Tests.Controllers {

    [TestFixture]
    public class DataPathControllerTests {
        
        [TestCase("~/Components/DataPath/View/DataPath.cshtml", true)]
        [TestCase("~/Components/Shared/Views/Error.cshtml", false)]
        public void BindDataPath_ReturnsDataPathView(string viewName, bool expectedResult) {
            var controller = new DataPathController();

            var result = controller.BindDataPath() as ViewResult;
            
            Assert.NotNull(result);
            Assert.That(string.Equals(result.ViewName, viewName) == expectedResult);
        }

        [Test]
        public void BindDataPath_ReturnsDataPathViewModel() {
            var controller = new DataPathController();

            var result = controller.BindDataPath() as ViewResult;

            Assert.NotNull(result);
            Assert.That(result.Model.GetType() == typeof(DataPathViewModel));
        }
    }
}

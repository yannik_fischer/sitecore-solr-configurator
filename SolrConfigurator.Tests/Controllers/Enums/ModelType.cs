﻿namespace SolrConfigurator.Tests.Controllers.Enums {
    public enum ModelType {
        Null = 0,
        Empty = 1,
        Populated = 2
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Moq;
using NUnit.Framework;
using SolrConfigurator.BL;
using SolrConfigurator.BL.Extensions;
using SolrConfigurator.Models.Serialization;
using SolrConfigurator.Tests.Controllers.Enums;
using SolrConfigurator.Tests.Mockup;
using SolrConfigurator.Website.Components.Export.Controller;
using SolrConfigurator.Website.Components.Selection.Model;
using Type = SolrConfigurator.Models.Enums.Type;

namespace SolrConfigurator.Tests.Controllers {

    [TestFixture]
    public class ExportControllerTests {
        private HttpContext _httpContext;
        private TempDataDictionary _tempData;
        private ExportController _controller;

        [SetUp]
        public void Setup() {
            _httpContext = new DefaultHttpContext();
            _tempData = new TempDataDictionary(_httpContext, Mock.Of<ITempDataProvider>());
            _controller = new ExportController(new FakeExportHandler()) {
                TempData = _tempData
            };
        }

        [TestCase("~/Components/Shared/Views/Error.cshtml", ModelType.Null, true)]
        [TestCase("~/Components/Shared/Views/Error.cshtml", ModelType.Empty, true)]
        [TestCase("~/Components/Shared/Views/Error.cshtml", ModelType.Populated, false)]
        [TestCase("~/Components/Export/View/Export.cshtml", ModelType.Null, false)]
        [TestCase("~/Components/Export/View/Export.cshtml", ModelType.Empty, false)]
        [TestCase("~/Components/Export/View/Export.cshtml", ModelType.Populated, true)]
        public void ProcessSelection_ReturnsExpectedView(string viewName, ModelType setModel, bool expectedResult) {
            SelectionViewModel viewModel = null;

            if (setModel == ModelType.Empty) {
                viewModel = new SelectionViewModel {
                    IncludedTemplates = new List<SerializedFile>(),
                    ExcludedTemplates = new List<SerializedFile>(),
                    IncludedFields = new List<SerializedFile>(),
                    ExcludedFields = new List<SerializedFile>(),
                    IndexAllFields = false
                };
            } else if (setModel == ModelType.Populated) {
                viewModel = new SelectionViewModel {
                    IncludedTemplates = new List<SerializedFile> {
                        new SerializedFile {
                            Name = "Entry 1",
                            Path = "sitecore/templates/Entry 1",
                            Type = Type.Template,
                            Value = Guid.Empty.ToString("D")
                        }
                    },
                    ExcludedTemplates = new List<SerializedFile> {
                        new SerializedFile {
                            Name = "Entry 2",
                            Path = "sitecore/templates/Entry 1",
                            Type = Type.Template,
                            Value = Guid.Empty.ToString("D")
                        }
                    },
                    IncludedFields = new List<SerializedFile> {
                        new SerializedFile {
                            Name = "Entry 3",
                            Path = "sitecore/templates/Entry 1",
                            Type = Type.Field,
                            Value = Guid.Empty.ToString("D")
                        }
                    },
                    ExcludedFields = new List<SerializedFile> {
                        new SerializedFile {
                            Name = "Entry 4",
                            Path = "sitecore/templates/Entry 1",
                            Type = Type.Field,
                            Value = Guid.Empty.ToString("D")
                        }
                    },
                    IndexAllFields = false
                };
            }

            _controller.TempData.Put(Constants.TempDataKeySelectionViewModel, viewModel);

            var result = _controller.ProcessSelection() as ViewResult;

            Assert.NotNull(result);
            Assert.That(string.Equals(result.ViewName, viewName) == expectedResult);
        }

        [TestCase(null, typeof(ViewResult))]
        [TestCase("<?xml version=\"1.0\" encoding=\"utf-8\" ?><configuration xmlns:patch=\"http://www.sitecore.net/xmlconfig/\"></configuration>", typeof(FileContentResult))]
        public void GetFileDownload_ReturnsExpectedResult(string config, System.Type expectedType) {
            _controller.TempData.Put(Constants.TempDataKeyGeneratedConfig, config);

            var result = _controller.GetFileDownload();

            Assert.NotNull(result);
            Assert.That(result.GetType() == expectedType);
        }
    }
}
